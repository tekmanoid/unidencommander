## UnidenCommander ##

UnidenCommander provides control over your Uniden Bearcat scanner from your PC. All scanner buttons are available through UnidenCommander. UnidenCommander even adds some unique features to your scanner.

Adaptations of the original UnidenCommander software by http://dx.torensma.net/

Original software supported UBC785XLT. An attempt to connect to a UBC780XLT resulted in an immediate crash / freeze of the program. This has been hereby fixed. Not all UBC780XLT features have been ported and lots of "TODO"s still exist for development!

* Quick summary
* V0.3.3 ORIGINAL
Supported Model: UBC785XLT ONLY!
* V0.3.4 this repo, LATEST
Supported Models: UBC785XLT + UBC780XLT !

* You need a RS232 cable to connect to the radio.
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* alex.scafidas@gmail.com