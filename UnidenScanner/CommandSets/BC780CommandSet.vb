﻿Imports System.Text.RegularExpressions

Namespace CommandSets

    Public Class BC780CommandSet
        Inherits GenericCommandSet
        Implements ICommandSet

        Public Sub New()

            ScannerModes(0) = New ScannerMode("SCAN", "Conventional scanning")
            ScannerModes(1) = New ScannerMode("MAN", "Manual")
            ScannerModes(2) = New ScannerMode("SRCH", "Limit Search")
            ScannerModes(3) = New ScannerMode("SRCH HOLD", "Limit Search Hold")
            ScannerModes(4) = New ScannerMode("SERVICE", "SERVICE Search")
            ScannerModes(5) = New ScannerMode("WEATHER HOLD", "Weather Search Hold")
            ScannerModes(6) = New ScannerMode("TRUNC", "Motorola Trunk Frequency Program")
            ScannerModes(7) = New ScannerMode("? (7)", "Unknown")
            ScannerModes(8) = New ScannerMode("TUNING", "Rotary Tune Frequency")
            ScannerModes(9) = New ScannerMode("MOT SRCH", "Motorola ID Search")
            ScannerModes(10) = New ScannerMode("MOT SRCH H", "Motorola ID Search Hold")
            ScannerModes(11) = New ScannerMode("MOT SCAN", "Motorola ID Scan")
            ScannerModes(12) = New ScannerMode("MOT MAN", "Motorola ID Manual")
            ScannerModes(13) = New ScannerMode("EDACS SRCH", "EDACS ID Search")
            ScannerModes(14) = New ScannerMode("EDACS HOLD", "EDACS ID Hold")
            ScannerModes(15) = New ScannerMode("EDACS SCAN", "EDACS ID Scan")
            ScannerModes(16) = New ScannerMode("EDACS MAN", "EDACS ID Manual")
            ScannerModes(17) = New ScannerMode("LTR SRCH", "LTR Search")
            ScannerModes(18) = New ScannerMode("LTR HOLD", "LTR Hold")
            ScannerModes(19) = New ScannerMode("LTR SCAN", "LTR Scan")
            ScannerModes(20) = New ScannerMode("LTR MAN", "LTR Manual")
            ScannerModes(21) = New ScannerMode("? (21)", "Unknown")

        End Sub

        ''' <summary>
        ''' Gets the screen refresh commands.
        ''' </summary>
        ''' <value>The screen refresh commands.</value>
        Public Overrides ReadOnly Property ScreenRefreshCommands() As List(Of Command)
            Get

                Dim cmd As New List(Of Command)
                Dim BC780Commands As CommandSets.BC780CommandSet = New CommandSets.BC780CommandSet

                cmd.Add(BC780Commands.LCDLine1)
                cmd.Add(BC780Commands.LCDLine2)
                cmd.Add(BC780Commands.SignalStrength)
                cmd.Add(BC780Commands.ReceiverModulation)
                cmd.Add(BC780Commands.Attenuator)
                cmd.Add(BC780Commands.Muting)
                cmd.Add(BC780Commands.StepSize)
                'cmd.Add(BC780Commands.WindowVoltage)
                cmd.Add(BC780Commands.Mode)
                cmd.Add(BC780Commands.ChannelNumber)
                cmd.Add(BC780Commands.LockOut)
                cmd.Add(BC780Commands.PriorityScan)
                cmd.Add(BC780Commands.PriorityChannel)
                'cmd.Add(BC780Commands.SquelchStatus)

                Return cmd

            End Get
        End Property

        Public Overrides ReadOnly Property ScannerModes() As Dictionary(Of Integer, ScannerMode)
            Get
                Return _scannerModes
            End Get
        End Property
        Public Overrides ReadOnly Property LockOut As Command
            Get
                Return New Command("LCD LOUT", New CallbackHandlerDelegate(AddressOf LockOutCallbackHandler))
            End Get
        End Property
        Public Overrides ReadOnly Property PriorityScan As Command
            Get
                Return New Command("LCD PRI", New CallbackHandlerDelegate(AddressOf PriorityScanCallbackHandler))
            End Get
        End Property
        Public Overrides ReadOnly Property PriorityChannel As Command
            Get
                Return New Command("LCD P", New CallbackHandlerDelegate(AddressOf PriorityChannelCallbackHandler))
            End Get
        End Property
        Public Overrides ReadOnly Property LCDLine1() As Command
            Get
                Return New Command("LCD LINE1", New CallbackHandlerDelegate(AddressOf Line1CallbackHandler))
            End Get
        End Property
        Public Overrides ReadOnly Property LCDLine2() As Command
            Get
                Return New Command("LCD LINE2", New CallbackHandlerDelegate(AddressOf Line2CallbackHandler))
            End Get
        End Property
        Public Overrides ReadOnly Property ChannelNumber() As Command
            Get
                Return New Command("LCD CHN", New CallbackHandlerDelegate(AddressOf Line1CallbackHandler))
            End Get
        End Property

        Protected Overloads Sub Line1CallbackHandler(ByVal response As String)
            Dim tmp As String
            tmp = Regex.Match(response, "LINE1\s\[(.{16})\]").Groups(1).Value
            If Not String.IsNullOrEmpty(tmp) Then
                ScannerReference.SetLCDLine3(tmp)       ' LINE1 is LINE3 here
            End If

            tmp = Regex.Match(response, "CHN\s\[\s*(\d{1,3})\]").Groups(1).Value
            If Not String.IsNullOrEmpty(tmp) Then
                Dim ch As Integer
                If Integer.TryParse(tmp, ch) Then
                    ScannerReference.SetChannel(ch)
                End If
            End If
        End Sub
        Protected Overloads Sub Line2CallbackHandler(ByVal response As String)
            Dim tmp As String
            tmp = Regex.Match(response, "LINE2\s\[(.{16})\]").Groups(1).Value
            If Not String.IsNullOrEmpty(tmp) Then
                ScannerReference.SetLCDLine4(tmp)       ' LINE2 is LINE4 here
            End If
        End Sub
        Protected Overloads Sub LockoutCallbackHandler(ByVal response As String)
            'LOUT -
            If Not String.IsNullOrEmpty(response) AndAlso response <> "NG" Then

                Dim att As String = response.Substring(5, 1)
                If att = "+" Then ScannerReference.SetLockOut(True)
                If att = "-" Then ScannerReference.SetLockOut(False)

            End If
        End Sub
        Protected Overloads Sub PriorityScanCallbackHandler(ByVal response As String)
            'PRI -
            If Not String.IsNullOrEmpty(response) AndAlso response <> "NG" Then

                Dim att As String = response.Substring(4, 1)
                If att = "+" Then ScannerReference.SetPriorityScan(True)
                If att = "-" Then ScannerReference.SetPriorityScan(False)

            End If
        End Sub
        Protected Overloads Sub PriorityChannelCallbackHandler(ByVal response As String)
            'P -
            If Not String.IsNullOrEmpty(response) AndAlso response <> "NG" Then

                Dim att As String = response.Substring(2, 1)
                If att = "+" Then ScannerReference.SetPriorityChannel(True)
                If att = "-" Then ScannerReference.SetPriorityChannel(False)

            End If
        End Sub
    End Class

End Namespace

